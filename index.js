const express = require('express')
const productRouter = require('./routes/product.routes')
const errorHandler = require('./middleware/ErrorHandlingMiddleware')
const fileUpoad = require('express-fileupload')
const app = express()
const PORT = process.env.PORT || 9000

app.use(express.json())
app.use(express.static('static'))
app.use(fileUpoad({}))
app.use('/api', productRouter)


app.use(errorHandler)

const start = () => {
    try {
        app.listen(PORT, () => console.log(`running on port: ${PORT}`))
    } catch (e) {
        console.log(e);
    }
}

start()