const db = require('../db')
const ApiError = require('../error/ApiError')
const fileController = require('./fileController')
const fs = require('fs')

class ProductController {
    async getAllProducts (req, res) {
        let {page} = req.query
        page = page || 1
        let offset = page * 9 - 9

        const products = await db.query('SELECT * FROM product OFFSET $1 LIMIT 9', [offset])
        res.json(products.rows)
    }

    async getProductById (req, res, next) {
        const { id } = req.query
        if(!id) {
            return next(ApiError.badRequest('Не задан id'))
        }
        const product = await db.query('SELECT * FROM product where id = $1', [id])
        res.json(product.rows[0])
    }

    async createProduct (req, res) {
        const fileName = fileController.saveFile(req.files.picture);
        const {title, price, remaining_amount} = req.body
        const product = await db.query('INSERT INTO product (title, price, remaining_amount, picture) values ($1, $2, $3, $4) RETURNING *',
        [title, price, remaining_amount, fileName])
        res.json(product.rows[0])
    }

    async updateProduct (req, res, next) {
        const {title, price, remaining_amount, id} = req.body
        if(!id || !title || !price || !remaining_amount) {
            return next(ApiError.badRequest('Не заданы все данные'))
        }
        const product = await db.query('UPDATE product set title = $1, price = $2, remaining_amount = $3 where id = $4 RETURNING *',
        [title, price, remaining_amount, id])
        res.json(product.rows[0])
    }

    async deleteProduct (req, res, next) {
        const { id } = req.query
        if(!id) {
            return next(ApiError.badRequest('Не задан id'))
        }
        const product = await db.query('SELECT * FROM product where id = $1', [id])
        fileController.deleteFile(product.rows[0].picture)

        await db.query('DELETE FROM product where id = $1', [id])
        res.json({ message: `product with id: ${id} was deleted`})
    }
}

module.exports = new ProductController()