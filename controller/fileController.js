const uuid = require('uuid')
const path = require('path')
const fs = require('fs')


class FileController {
    saveFile(file) {
        try {
            const fileName = uuid.v4() + '.jpg'
            const filePath = path.resolve('static', fileName)
            file.mv(filePath)
            return fileName
        } catch (e) {
            console.log(e);
        }
    }

    deleteFile(picture) {
        try {
            if(fs.existsSync(`static/${picture}`)) {
                fs.unlinkSync(`static/${picture}`)
            } else {
                console.log('The file does not exist.');
            }
        } catch (err) {
            console.error(err);
        }
        
    }
}

module.exports = new FileController()