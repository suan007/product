const Router = require('express')
const productController = require('../controller/product.controller')
const router = new Router()

router.get('/products', productController.getAllProducts)
router.get('/product', productController.getProductById)
router.post('/products', productController.createProduct)
router.put('/product', productController.updateProduct)
router.delete('/product', productController.deleteProduct)

module.exports = router

